var gulp = require('gulp');

const
    resourcesDirectory = "resources",
    artifactsDirectory = "../app/target/",
    distributionDirectory = "../app/target";

gulp.task('deploy', function() {
    copy(resourcesDirectory + '/so/osx/AfirmaSkeleton.app/**/*', [distributionDirectory + "/AutoFirma1.4.1.app"]);
    copy(artifactsDirectory + '/AutoFirma_1.4.jar', [distributionDirectory + "/AutoFirma1.4.1.app/Contents/Resources"]);
});

gulp.task('default', ['deploy']);

function copy(src, dests) {
    dests.forEach(function (dest) {
        gulp.src(src).pipe(gulp.dest(dest));
    });
}
